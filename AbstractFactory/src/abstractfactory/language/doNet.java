/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory.language;

import abstractfactory.interfaces.IApplication;

/**
 *
 * @author jorge
 */
public class doNet implements IApplication{

    @Override
    public void desktop() {
        System.out.println("Se desarrollo una aplicación de Windows");
    }

    @Override
    public void web() {        
        System.out.println("Se desarrollo una aplicación en ASP.net");
    }

    @Override
    public void mobile() {
        
        System.out.println("Se desarrollo una aplicación en Xamarin");
    }
    
}
