/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory.fabricas;

import abstractfactory.interfaces.AppFactory;

/**
 *
 * @author jorge
 */
public class AbstractFactory {
    public AppFactory obtenerFabrica(String lenguaje){
        if(lenguaje == null || lenguaje.equalsIgnoreCase("java")){
            return new JavaFactory();
        }else{
            return new doNetFactory();
        }
    }
}
