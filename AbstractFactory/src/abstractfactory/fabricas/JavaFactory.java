/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory.fabricas;

import abstractfactory.interfaces.AppFactory;
import abstractfactory.interfaces.IApplication;
import abstractfactory.language.Java;

/**
 *
 * @author jorge
 */
public class JavaFactory implements AppFactory{

    @Override
    public IApplication crearApp() {
        return new Java();
    }
    
}
