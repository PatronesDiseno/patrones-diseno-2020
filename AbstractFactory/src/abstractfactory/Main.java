/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory;

import abstractfactory.fabricas.AbstractFactory;
import abstractfactory.interfaces.AppFactory;
import abstractfactory.interfaces.IApplication;

/**
 *
 * @author jorge
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AbstractFactory fabricaAbstracta = new AbstractFactory();
        crearApp(fabricaAbstracta.obtenerFabrica("no se que lenguaje elegir"));
    }
    
    public static void crearApp(AppFactory factory){
        IApplication app = factory.crearApp();
        System.out.println("========================");
        app.desktop();
        app.web();
        app.mobile();
    }
    
}
