/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractfactory.interfaces;

/**
 *
 * @author jorge
 */
public interface IApplication {
    public void desktop();
    public void web();
    public void mobile();
}
