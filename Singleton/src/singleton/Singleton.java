/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author jorge
 */
public class Singleton {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Paises p1 = Paises.getInstance();
        
        Paises p2 = Paises.getInstance();
        
        p1.mostrarPais();
        p2.mostrarPais();
        
        p1.setNombres("Africa");
        
        p1.mostrarPais();
        p2.mostrarPais();
        
        boolean res = p1 instanceof Paises;
        boolean res2 = p2 instanceof Paises;
        
        System.out.println("Resultado de usuario 1:"+res);
        System.out.println("Resultado de usuario 2:"+res2);
        
        
    }
    
}
