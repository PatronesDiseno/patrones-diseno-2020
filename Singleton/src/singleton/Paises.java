/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton;

/**
 *
 * @author jorge
 */
public class Paises {
    //Instancia que se obtendra publica
    //por medio de un metodo de acceso
    private static Paises instancia;
    
    //Atributos
    private String nombres;
    
    //Constructor
    private Paises(){
        nombres = "México";
    }
    
    //Metodo para la instacia.
    public static Paises getInstance(){
        if(instancia == null){
            instancia = new Paises();
        }
        
        return instancia;
    }
    
    //Metodo para imprimir valores
    public void mostrarPais(){
        System.out.println("El pais almacenado es :"+nombres);
    }

    //Getters && Setters
    
    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
}
