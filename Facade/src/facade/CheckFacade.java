/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facade;

import api.AvionAPI;
import api.HotelAPI;

/**
 *
 * @author jorge
 */
public class CheckFacade {
    
    private AvionAPI avionApi;
    private HotelAPI hotelApi;

    public CheckFacade() {
        avionApi = new AvionAPI();
        hotelApi = new HotelAPI();
    }
    
    public void buscar(String fechaSalida,String fechaRegreso, String origen, String destino){
        avionApi.obtenerVuelos(fechaSalida, fechaRegreso, origen, destino);
        
        /*
            parametro    => Equivalencia
            fechaSalida  => fechaEntrada (en el hotel)
            fechaRegreso => fechaSalida  (en el hotel)
        */
        hotelApi.obetnerHotel(fechaSalida, fechaRegreso, origen, destino);
    }
    
    
}
