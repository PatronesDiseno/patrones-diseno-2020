/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factory.implementacion;

import factory.interfaces.IFigura;

/**
 *
 * @author jorge
 */
public class Cuadrado implements IFigura {

    @Override
    public void dibujar() {
        System.out.println("Ha dibujado un Cuadrado!");
    }
    
}
