/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package factory;

import factory.implementacion.Circulo;
import factory.implementacion.Cuadrado;
import factory.implementacion.Rectangulo;
import factory.interfaces.IFigura;

/**
 *
 * @author jorge
 */
public class FiguraFactory {
    public IFigura getFigura(String tipo){
        if(tipo == null){
            return null;
        }
        
        if(tipo.equalsIgnoreCase("circulo")){
            return new Circulo();
        }
        
        if(tipo.equalsIgnoreCase("cuadrado")){
            return new Cuadrado();
        }
        
        if(tipo.equalsIgnoreCase("rectangulo")){
            return new Rectangulo();
        }
        
        return null;
    }
}
